# Sentiment Analysis 

The model is built from scratch but inspired from [this](http://peterbloem.nl/blog/transformers) and trained on the [Sentiment140 dataset](https://www.kaggle.com/kazanova/sentiment140).
